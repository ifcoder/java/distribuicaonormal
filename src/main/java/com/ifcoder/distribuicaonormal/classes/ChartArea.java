/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcoder.distribuicaonormal.classes;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 *
 * @author jose
 */
public class ChartArea {

    private JFreeChart chart;
    private DefaultCategoryDataset datasetAux;
    private JPanel panGrafico;

    public ChartArea() {
        datasetAux = new DefaultCategoryDataset();
    }

    public ChartArea(JPanel panToShowGrafico) {
        this();
        panGrafico = panToShowGrafico;
        this.criarGrafico();
    }      
    
    public double funcao(double x){
        double y = 0;
        y = x*x*x -10*x +90;
        return y;
    }
    
    
    public double funcaoLogNormal2(double x, double media, double desvioPadrao){
        double y, A, B, C;
        A = 1.0/(x*desvioPadrao*Math.sqrt(2.0 *Math.PI));
        B = (double)(Math.log(x) - media)/desvioPadrao;
        B = -0.5*(B*B);
        C = Math.exp(B);
        y =  A*C;
        return y;
    }    
    
    public double funcaoNormal(double x, double media, double desvioPadrao){
        double y, A, B, C, D;
        A = 1.0/(desvioPadrao*Math.sqrt(2.0 *Math.PI));
        C = -1.0 *(x-media)*(x -media);
        D = 2.0 * desvioPadrao*desvioPadrao;
        B = (double) C/D;
        y=  A*Math.exp(B);
        return y;
    }
    
    
    public double preencheSerie(double media, double desvioPadrao, double inferior, double superior, int qtdDados){
        final String nomeSerie = "Distribuição normal";   
        datasetAux.clear();
        
        double passo = (double) (superior - inferior)/qtdDados;
        double soma = 0;
        double i = inferior;
        double contador=1;
        while( contador <= qtdDados+1){
            double y = funcaoNormal(i, media, desvioPadrao);
            datasetAux.addValue(y,nomeSerie, i+"");
            soma += passo*y;                                               
           // System.out.println("i:"+contador+ ", x:"+i + ", y:"+ y);            
            i += passo;
            contador++;
        }        
        return soma;
    }    
    
    private void criarGrafico() {
        final JFreeChart chart = ChartFactory.createAreaChart(
            "Área",             // chart title
            "",               // domain axis label
            "f(x)",                  // range axis label
            datasetAux,                  // data
            PlotOrientation.VERTICAL, // orientation
            true,                     // include legend
            true,                     // tooltips
            false                     // urls
        );        
       
        ChartPanel chPanel = new ChartPanel(chart); //creating the chart panel, which extends JPanel
        chPanel.setPreferredSize(new Dimension(1024, 500)); //size according to my window
//        chPanel.setMouseWheelEnabled(true);

        //Adicionando no PANEL
        getPanGrafico().setLayout(new java.awt.BorderLayout());
        getPanGrafico().add(chPanel, BorderLayout.CENTER);
        getPanGrafico().validate();
    }

    /**
     * @return the panGrafico
     */
    public JPanel getPanGrafico() {
        return panGrafico;
    }

    /**
     * @param panGrafico the panGrafico to set
     */
    public void setPanGrafico(JPanel panGrafico) {
        this.panGrafico = panGrafico;
    }

}
