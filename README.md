# Sistema de Distribuição Normal: Aplicação Java Swing com Maven

Este projeto é uma aplicação desktop desenvolvida em Java Swing, utilizando o Maven para gerenciamento de dependências. O foco principal é a implementação e visualização da distribuição normal, um conceito fundamental em estatística.

![Abordagens utilizadas](./src/main/resources/imagens/tela_distribuicaoNormal.png)

## Implementação da Distribuição Normal

- **Cálculos Estatísticos**: Funções para cálculo da distribuição normal e outras operações estatísticas.
- **Visualização de Dados**: Interface gráfica para representação gráfica dos resultados estatísticos.
- **Interatividade**: Ferramentas interativas para manipulação e análise de dados.

## Estrutura MVC

A adoção do padrão MVC oferece uma clara separação entre lógica de negócios, interface do usuário e controle de eventos, garantindo uma arquitetura limpa e organizada.

